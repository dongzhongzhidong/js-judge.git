import { defineConfig } from "vite";
import solidPlugin from "vite-plugin-solid";
import visualizer from "rollup-plugin-visualizer";
import { nodePolyfills } from "vite-plugin-node-polyfills";

export default defineConfig((config) => {
    const { mode } = config;
    console.log(mode);
    return {
        plugins: [
            solidPlugin(),
            nodePolyfills({
                // Whether to polyfill `node:` protocol imports.
                protocolImports: true,
            }),
            mode === "analyze" &&
                (visualizer({
                    open: true,
                    filename: "visualizer/stat.html",
                }) as any),
        ],
        resolve: {
            alias: {
                "rollup-web":
                    "https://cdn.jsdelivr.net/npm/rollup-web/dist/index.js",

                "rollup-web/": "https://cdn.jsdelivr.net/npm/rollup-web/",
                "@forsee/blockly": "/packages/blockly/src/index.ts",
            },
        },
        server: {
            port: 3000,
        },
        worker: {
            format: "es",
        },
    };
});
