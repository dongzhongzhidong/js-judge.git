import babel from "@rollup/plugin-babel";
import nodeResolve from "@rollup/plugin-node-resolve";
import fs, { writeFileSync } from "fs-extra";
import postcss from "rollup-plugin-postcss";
import commonjs from "@rollup/plugin-commonjs";
// import { terser } from "rollup-plugin-terser";
import analyze from "rollup-plugin-analyzer";
import { readFileSync } from "fs";

fs.emptyDir("./dist/");
// rollup.config.js
export default {
    external: [
        "solid-js",
        "solid-split-component",
        "solid-js/web",
        "rollup-web",
    ],
    input: "./src/Previewer.tsx",
    output: {
        dir: "./dist/",
        format: "es",
        paths: {},
    },
    plugins: [
        {
            resolveId(thisFile) {
                if (thisFile.startsWith("monaco-editor-solid")) {
                    return false;
                }
                if (thisFile.startsWith("https://")) {
                    return false;
                }
            },
        },
        {
            load(id) {
                if (id.endsWith(".svg")) {
                    const code = readFileSync(id, "utf-8");
                    return {
                        code: `
                        export default (${JSON.stringify(code)})`,
                    };
                }
            },
        },
        nodeResolve({
            browser: true,
            extensions: [".ts", ".tsx", ".js"],
        }),
        commonjs(),

        postcss({
            inject: true,
            minimize: {},
            modules: {},
            sourceMap: false,
            extensions: [".css", ".less"],
        }),
        babel({
            babelrc: true,
            babelHelpers: "bundled",
            extensions: [".ts", ".tsx", ".js", ".svg"],
        }),

        // terser(),
        analyze({
            summaryOnly: true,
            writeTo: (str) => writeFileSync("dist/index.analyze.txt", str),
        }),
    ],
};
