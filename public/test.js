import { describe, it } from "@forsee/mocha";
import { expect } from "chai@4.3.7";
describe("测试", () => {
    it("第一次测试", () => {
        expect(2).eq(1);
    });
    it("第二次测试", () => {
        expect(1 + 1).lessThanOrEqual(2);
    });
});
