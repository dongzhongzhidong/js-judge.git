import { expose, releaseProxy } from "comlink";
import { EventEmitter } from "events";
export class TestMachine extends EventEmitter {
    init(port: MessagePort) {
        expose(
            {
                onEnd() {
                    console.log("end of test");
                },
                onChange(data) {
                    console.log(data);
                },
            },
            port
        );
    }

    destroy() {}
}
