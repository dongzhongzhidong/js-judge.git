export class StepsCounter {
    readonly steps = [
        {
            key: "",
            label: "加载",
        },
    ] as const;

    private now = 0;
    get nowItem() {
        return this.steps[this.now];
    }
    record(stepKey: string) {
        this.steps[this.now + 1].key === stepKey;
    }
    reset() {
        this.now = 0;
    }
}
