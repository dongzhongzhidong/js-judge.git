import { CompilerManager } from "./CompilerManager";
import { FileStore } from "../interface";
import { TestMachine } from "../Test";

export class RollupWebRuntime {
    Compiler: CompilerManager;
    constructor(public fileStore: FileStore, public test: TestMachine) {
        this.Compiler = new CompilerManager({
            fileStore,
        });
    }
    init(config: any) {}
    async build(): Promise<void> {}

    async reload(): Promise<void> {}
    async destroy(): Promise<void> {
        this.Compiler.destroy();
    }
}
