import mitt from "mitt";
import { Console } from "@forsee/console/dist/Console";
import { ConsoleView } from "@forsee/console";

class ConsoleHub {
    hub = mitt<{
        update: Parameters<ConsoleView["insertSync"]>;
        clear: null;
    }>();
    createConsole(iframe: HTMLIFrameElement) {
        const _c = new Console();

        (iframe.contentWindow! as any).console = _c;

        _c.init(
            (...args) => this.hub.emit("update", args),
            () => this.hub.emit("clear", null),
            "classic"
        );
    }
}
