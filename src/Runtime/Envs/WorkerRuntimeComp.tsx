import { onMount, useContext } from "solid-js";
import { CodingSystemContext } from "../../CodingApp";
import { WorkerRuntime } from "./WorkerRuntime";
import { TestMachine } from "../../Test";

export const WorkerRuntimeComp = () => {
    const { store, runtime } = useContext(CodingSystemContext);
    let container: HTMLDivElement;
    onMount(() => {
        const Test = new TestMachine();
        const _runtime = new WorkerRuntime(store, Test);
        _runtime.init({ container });
        runtime(_runtime);
        // runtime.build();
    });
    return null;
};
