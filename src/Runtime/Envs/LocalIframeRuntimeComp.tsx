import { onMount, useContext } from "solid-js";
import { CodingSystemContext } from "../../CodingApp";
import { LocalIframeRuntime } from "./LocalIframeRuntime";

export const LocalIframeRuntimeComp = () => {
    const { store, runtime } = useContext(CodingSystemContext);
    let container: HTMLDivElement;
    onMount(() => {
        const _runtime = new LocalIframeRuntime(store);
        _runtime.init({ container });
        runtime(_runtime);
        // runtime.build();
    });
    return <div ref={container}></div>;
};
