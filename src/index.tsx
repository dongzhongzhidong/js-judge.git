import { render } from "solid-js/web";
import { CodingApp } from "./CodingApp";
import "./index.css";
import "./icon.css";
const App = () => {
    return (
        <div>
            <CodingApp></CodingApp>
        </div>
    );
};
render(App, document.body);
