import FS from "@isomorphic-git/lightning-fs";
import { LightningStore } from "./TextEditor";
import { resource } from "@cn-ui/use";

export const useSourceCodeStore = ({
    loadCodeFromRemote,
}: {
    loadCodeFromRemote(): Promise<[string, string][]>;
}) => {
    const fs = new FS();
    fs.init("first-test");
    const store = new LightningStore(fs);
    const storeReady = resource(async () => {
        const codes = await loadCodeFromRemote();
        return Promise.all(
            codes.map(async ([path, code]) => {
                return fs.promises.writeFile(path, code);
            })
        );
    });
    return {
        /**
         * @deprecated
         * */
        fs,
        store,
        storeReady,
    };
};
