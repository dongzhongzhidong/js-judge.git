import { useBlockly, initModuleBlocks } from "@forsee/blockly";
import { Component, Show, onMount, useContext } from "solid-js";
import { CodingSystemContext } from "../CodingApp";
import { atom } from "@cn-ui/use";
import { IconWrapper } from "monaco-editor-solid";
export const Blockly: Component<{
    onReady?: (
        system: ReturnType<typeof useBlockly>,
        container: HTMLDivElement
    ) => void;
}> = (props) => {
    let container: HTMLDivElement;
    onMount(() => {
        const sys = useBlockly(container);
        initModuleBlocks();
        props.onReady && props.onReady(sys, container);
    });

    return <div ref={container} class="h-full w-full"></div>;
};
import { debounce } from "lodash-es";
export const BlocklyComp = () => {
    const { store, explorerVisible, editorOpenFile } =
        useContext(CodingSystemContext);

    const openedPath = atom<string>(null);
    const editingCode = atom("");
    return (
        <>
            <main class="flex flex-col flex-1">
                <header class="flex items-center px-2">
                    <span onclick={() => explorerVisible((i) => !i)}>
                        <IconWrapper name="list-tree"></IconWrapper>
                    </span>
                    <div class="flex-1 text-center">File Editor</div>
                    <span
                        onclick={() => {
                            openedPath(null);
                            explorerVisible(true);
                        }}
                    >
                        <IconWrapper name="split-horizontal"></IconWrapper>
                    </span>
                </header>

                <Blockly
                    onReady={(
                        { loadFromString, getFile, autoResize, getCode },
                        container
                    ) => {
                        autoResize();
                        const onChange = debounce(() => {
                            console.log("changing", openedPath());

                            editingCode(getCode());
                            if (openedPath()) {
                                store.setFileString(openedPath(), getFile());
                            }
                        }, 300);
                        container.addEventListener("pointerup", onChange);
                        container.addEventListener("pointerleave", onChange);
                        container.addEventListener("pointercancel", onChange);

                        editorOpenFile(() => {
                            return (path) => {
                                if (path.endsWith(".block")) {
                                    openedPath(path);
                                    store.getFileString(path).then((code) => {
                                        loadFromString(code);
                                        editingCode(getCode());
                                    });
                                } else {
                                    console.log("请点击一个 block 文件");
                                }
                            };
                        });
                    }}
                ></Blockly>
            </main>
            <div class="w-40">{editingCode()}</div>
        </>
    );
};
