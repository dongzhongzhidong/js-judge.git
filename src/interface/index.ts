export abstract class BaseEditor {
    abstract getCode(): string | Promise<string>;
    abstract setCode(code: string): void;
    source: FileStore;
}

type TestResult = {}[];
export abstract class JudgeServer {
    upload: (files: FileStore) => Promise<TestResult>;
}

export abstract class FileStore {
    abstract getFile(path: string): Promise<Blob>;
    abstract getFileString(path: string): Promise<string>;
    abstract setFile(path: string, val: Blob): Promise<void>;
    abstract setFileString(path: string, val: string): Promise<void>;
    abstract moveFile(path: string, to: string): Promise<void>;
    abstract deleteFile(path: string): Promise<void>;
}
