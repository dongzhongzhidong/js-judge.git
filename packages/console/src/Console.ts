import isUndef from "licia/isUndef";
import perfNow from "licia/perfNow";
import isStr from "licia/isStr";
import isEmpty from "licia/isEmpty";
import copy from "licia/copy";
import each from "licia/each";
import xpath from "licia/xpath";
import { ConsoleView } from "./ConsoleView";
import { ILogOptions } from "./Log";
// import { getCurTime, getFrom } from "./util/util";

export type InsertOptions = Partial<ILogOptions> & {
    type: string;
    args: any[];
};
import keys from "licia/keys";

export class Console {
    private timer: { [key: string]: number } = {};
    private counter: { [key: string]: number } = {};

    private global = {
        copy(value: string) {
            if (!isStr(value)) value = JSON.stringify(value, null, 2);
            copy(value);
        },
        $(selectors: string) {
            return document.querySelector(selectors);
        },
        $$(selectors: string) {
            return [...document.querySelectorAll(selectors)];
        },
        $x(path: string) {
            return xpath(path);
        },
        clear: () => {
            this.clear();
        },
        dir: (value: any) => {
            this.dir(value);
        },
        table: (data: any, columns: any) => {
            this.table(data, columns);
        },
        keys,
        $_: undefined as any,
    };
    constructor() {
        // https://developers.google.cn/web/tools/chrome-devtools/console/utilities
    }
    public updateView: ConsoleView["insertSync"] = () => {};
    public clearView: (silent?: boolean) => void = () => {};
    init(
        updateView: Console["updateView"],
        clearView: Console["clearView"],
        mode?: Console["mode"]
    ) {
        this.updateView = updateView;
        this.clearView = clearView;
        mode && (this.mode = mode);
    }
    setGlobal(name: keyof Console["global"], val: any) {
        this.global[name] = val;
    }

    count(label = "default") {
        const { counter } = this;

        !isUndef(counter[label]) ? counter[label]++ : (counter[label] = 1);

        this.info(`${label}: ${counter[label]}`);
    }
    countReset(label = "default") {
        this.counter[label] = 0;
    }
    assert(...args: any[]) {
        if (isEmpty(args)) return;

        const exp = args.shift();

        if (!exp) {
            if (args.length === 0) args.unshift("console.assert");
            args.unshift("Assertion failed: ");
            this.insert("error", args);
        }
    }
    log(...args: any[]) {
        if (isEmpty(args)) return;

        this.insert("log", args);
    }
    debug(...args: any[]) {
        if (isEmpty(args)) return;

        this.insert("debug", args);
    }
    dir(obj: any) {
        if (isUndef(obj)) return;

        this.insert("dir", [obj]);
    }
    table(...args: any[]) {
        if (isEmpty(args)) return;

        this.insert("table", args);
    }
    time(name = "default") {
        if (this.timer[name]) {
            return this.insert("warn", [`Timer '${name}' already exists`]);
        }
        this.timer[name] = perfNow();
    }
    timeLog(name = "default") {
        const startTime = this.timer[name];

        if (!startTime) {
            return this.insert("warn", [`Timer '${name}' does not exist`]);
        }

        this.info(`${name}: ${perfNow() - startTime}ms`);
    }
    timeEnd(name = "default") {
        this.timeLog(name);

        delete this.timer[name];
    }
    clear(silent = false) {
        this.counter = {};
        this.timer = {};
        this.clearView(silent);
    }
    info(...args: any[]) {
        if (isEmpty(args)) return;

        this.insert("log", args);
    }
    error(...args: any[]) {
        if (isEmpty(args)) return;

        this.insert("error", args);
    }
    warn(...args: any[]) {
        if (isEmpty(args)) return;

        this.insert("warn", args);
    }
    group(...args: any[]) {
        this.insert({
            type: "group",
            args,
            ignoreFilter: true,
        });
    }
    groupCollapsed(...args: any[]) {
        this.insert({
            type: "groupCollapsed",
            args,
            ignoreFilter: true,
        });
    }
    groupEnd() {
        this.insert("groupEnd");
    }
    /** Evaluate JavaScript. */
    evaluate(code: string) {
        this.insert({
            type: "input",
            args: [code],
            ignoreFilter: true,
        });

        try {
            this.output(this.evalJs(code));
        } catch (e) {
            this.insert({
                type: "error",
                ignoreFilter: true,
                args: [e],
            });
        }
    }
    private evalJs(jsInput: string) {
        let ret;

        this.injectGlobal();
        try {
            ret = eval.call(globalThis, `(${jsInput})`);
        } catch (e) {
            ret = eval.call(globalThis, jsInput);
        }
        this.setGlobal("$_", ret);
        this.clearGlobal();

        return ret;
    }
    private injectGlobal() {
        each(this.global, (val, name) => {
            if ((globalThis as any)[name]) return;
            (globalThis as any)[name] = val;
        });
    }
    private clearGlobal() {
        each(this.global, (val, name) => {
            if (
                (globalThis as any)[name] &&
                (globalThis as any)[name] === val
            ) {
                delete (globalThis as any)[name];
            }
        });
    }
    /** Log out html content. */
    html(...args: any) {
        this.insert("html", args);
    }

    private output(val: string) {
        this.insert({
            type: "output",
            args: [val],
            ignoreFilter: true,
        });
    }
    mode: "classic" | "worker" = "classic";
    private insert(type: string | InsertOptions, args?: any[]) {
        let time;
        //  time = {
        //     time: getCurTime(),
        //     from: getFrom(),
        // }
        switch (this.mode) {
            case "classic":
                return this.updateView(type, args, time || undefined);
            case "worker":
                try {
                    /* @ts-ignore */
                    return this.updateView(type, args && JSON.stringify(args));
                } catch (e) {
                    /* @ts-ignore */
                    return this.updateView(type, JSON.stringify(args));
                }
        }
    }
}
