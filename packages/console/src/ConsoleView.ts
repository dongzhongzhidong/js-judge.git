import Log, { IGroup, IHeader, ILogOptions } from "./Log";
import now from "licia/now";
import isStr from "licia/isStr";
import extend from "licia/extend";
import uniqId from "licia/uniqId";
import isRegExp from "licia/isRegExp";
import isFn from "licia/isFn";
import $ from "licia/$";
import Stack from "licia/Stack";
import contain from "licia/contain";
import last from "licia/last";
import throttle from "licia/throttle";
import lowerCase from "licia/lowerCase";
import isHidden from "licia/isHidden";
import stripIndent from "licia/stripIndent";
import ResizeSensor from "licia/ResizeSensor";
import types from "licia/types";
import Component from "./util/Component";

import type { IOptions } from "./index";
import type { InsertOptions } from "./Console";
import { c } from "./util/classTrans";
import { htmlC } from "./util/util";
// export const c = (a: string) => a;
const u = navigator.userAgent;
const isAndroid = u.indexOf("Android") > -1 || u.indexOf("Adr") > -1;
let id = 0;
export class ConsoleView extends Component<IOptions> {
    constructor(container: HTMLElement, options: IOptions = {}) {
        super(container, { compName: "console" }, options);
        Object.assign(this, { options });
        this.initTpl();

        this.initOptions(options, {
            maxNum: 0,
            asyncRender: true,
            showHeader: false,
            filter: "all",
            accessGetter: false,
            unenumerable: true,
            lazyEvaluation: true,
        });

        this.$el = this.find(".logs");
        this.el = this.$el.get(0) as HTMLElement;
        this.$fakeEl = this.find(".fake-logs");
        this.fakeEl = this.$fakeEl.get(0) as HTMLElement;
        this.$space = this.find(".logs-space");
        this.space = this.$space.get(0) as HTMLElement;

        // For android slowing rendering
        if (isAndroid) {
            this.speedToleranceFactor = 800;
            this.maxSpeedTolerance = 3000;
            this.minSpeedTolerance = 800;
        }

        this.resizeSensor = new ResizeSensor(container);

        this.renderViewport = throttle((options) => {
            this._renderViewport(options);
        }, 16);
        this.bindEvent();
    }
    clear(silent = false) {
        this.emit("clear");
        this.logs = [];
        this.displayLogs = [];
        this.lastLog = undefined;
        this.groupStack = new Stack();
        this.render();
        !silent &&
            this.insertSync("log", [
                "%cConsole was cleared",
                "color:#808080;font-style:italic;",
            ]);
    }
    destroy() {
        this.$container.off("scroll", this.onScroll);
        this.resizeSensor.destroy();
    }
    renderViewport: (options?: any) => void;
    private $el: $.$;
    private $fakeEl: $.$;
    private $space: $.$;
    private resizeSensor: ResizeSensor;
    private el: HTMLElement;
    private fakeEl: HTMLElement;
    private space: HTMLElement;
    private spaceHeight = 0;
    private topSpaceHeight = 0;
    private lastScrollTop = 0;
    private lastTimestamp = 0;
    private speedToleranceFactor = 100;
    private maxSpeedTolerance = 2000;
    private minSpeedTolerance = 100;

    private logs: Log[] = [];

    insertSync(type: string | InsertOptions, args?: any[], header?: IHeader) {
        const { logs, groupStack } = this;

        const {
            maxNum,
            accessGetter,
            unenumerable: numerable,
            lazyEvaluation,
        } = this.options;
        let options: InsertOptions;
        if (isStr(type)) {
            options = {
                type: type as string,
                args: args as any[],
                header,
            };
        } else {
            options = type as InsertOptions;
        }

        // Because asynchronous rendering, groupEnd must be put here.
        if (options.type === "groupEnd") {
            const lastLog = this.lastLog as Log;
            lastLog.groupEnd();
            this.groupStack.pop();
            return;
        }

        if (groupStack.size > 0) {
            options.group = groupStack.peek();
        }
        extend(options, {
            id: ++id,
            accessGetter,
            numerable: numerable,
            lazyEvaluation,
        });

        if (options.type === "group" || options.type === "groupCollapsed") {
            const group = {
                id: uniqId("group"),
                collapsed: false,
                parent: groupStack.peek(),
                indentLevel: groupStack.size + 1,
            };
            if (options.type === "groupCollapsed") group.collapsed = true;
            options.targetGroup = group;
            groupStack.push(group);
        }

        let log = new Log(this, options as ILogOptions);
        log.on("updateSize", () => {
            this.isAtBottom = false;
            this.renderViewport();
        });

        const lastLog = this.lastLog;
        if (
            lastLog &&
            !contain(["html", "group", "groupCollapsed"], log.type) &&
            lastLog.type === log.type &&
            !log.src &&
            !log.args &&
            lastLog.text() === log.text()
        ) {
            lastLog.addCount();
            if (log.header) lastLog.updateTime(log.header.time);
            log = lastLog;
            this.detachLog(lastLog);
        } else {
            logs.push(log);
            this.lastLog = log;
        }

        if (maxNum !== 0 && logs.length > maxNum) {
            const firstLog = logs[0];
            this.detachLog(firstLog);
            logs.shift();
        }

        this.attachLog(log);

        this.emit("insert", log);
    }
    private updateTopSpace(height: number) {
        this.topSpaceHeight = height;
        this.el.style.top = height + "px";
    }
    private updateBottomSpace(height: number) {
        this.bottomSpaceHeight = height;
    }
    bottomSpaceHeight = 0;

    private updateSpace(height: number) {
        if (this.spaceHeight === height) return;
        this.spaceHeight = height;
        this.space.style.height = height + "px";
    }
    private detachLog(log: Log) {
        const { displayLogs } = this;

        const idx = displayLogs.indexOf(log);
        if (idx > -1) {
            displayLogs.splice(idx, 1);
            this.renderViewport();
        }
    }
    // Binary search
    private attachLog(log: Log) {
        // 没有 filter 并且是 没有折叠才能够继续渲染
        if (!this.filterLog(log) || log.collapsed) return;
        const { displayLogs } = this;

        if (displayLogs.length === 0) {
            displayLogs.push(log);
            this.renderViewport();
            return;
        }

        const lastDisplayLog = last(displayLogs);
        if (log.id > lastDisplayLog.id) {
            displayLogs.push(log);
            this.renderViewport();
            return;
        }

        let startIdx = 0;
        let endIdx = displayLogs.length - 1;

        let middleLog: any;
        let middleIdx = 0;

        while (startIdx <= endIdx) {
            middleIdx = startIdx + Math.floor((endIdx - startIdx) / 2);
            middleLog = displayLogs[middleIdx];

            if (middleLog.id === log.id) {
                return;
            }

            if (middleLog.id < log.id) {
                startIdx = middleIdx + 1;
            } else {
                endIdx = middleIdx - 1;
            }
        }

        if (middleLog.id < log.id) {
            displayLogs.splice(middleIdx + 1, 0, log);
        } else {
            displayLogs.splice(middleIdx, 0, log);
        }

        this.renderViewport();
    }

    private filterLog(log: Log) {
        const { filter } = this.options;

        if (filter === "all") return true;

        if (log.ignoreFilter) {
            return true;
        }

        if (isFn(filter)) {
            return (filter as types.AnyFn)(log);
        }

        if (isRegExp(filter)) {
            return (filter as RegExp).test(lowerCase(log.text()));
        }

        return log.type === filter;
    }
    private collapseGroup(log: Log) {
        const { targetGroup } = log;
        (targetGroup as IGroup).collapsed = true;
        log.updateIcon("caret-right");

        this.updateGroup(log);
    }
    private openGroup(log: Log) {
        const { targetGroup } = log;
        (targetGroup as IGroup).collapsed = false;
        log.updateIcon("caret-down");

        this.updateGroup(log);
    }
    private render() {
        const { logs } = this;

        this.$el.html("");
        this.isAtBottom = true;
        this.updateBottomSpace(0);
        this.updateTopSpace(0);
        this.displayLogs = [];
        for (let i = 0, len = logs.length; i < len; i++) {
            this.attachLog(logs[i]);
        }
    }
    toggleGroup(log: Log) {
        const { targetGroup } = log;
        (targetGroup as IGroup).collapsed
            ? this.openGroup(log)
            : this.collapseGroup(log);
    }
    private updateGroup(log: Log) {
        const { targetGroup } = log;
        const { logs } = this;
        const len = logs.length;
        let i = logs.indexOf(log) + 1;
        while (i < len) {
            const log = logs[i];
            if (!log.checkGroup() && log.group === targetGroup) {
                break;
            }
            log.collapsed ? this.detachLog(log) : this.attachLog(log);
            i++;
        }
    }
    private bindEvent() {
        const { $el } = this;

        this.resizeSensor.addListener(this.renderViewport);

        $el.on("click", c(".log-container"), function (this: any) {
            this.log.click();
        });

        this.on("optionChange", (name, val) => {
            const { logs } = this;
            switch (name) {
                case "maxNum":
                    if (val > 0 && logs.length > val) {
                        this.logs = logs.slice(logs.length - (val as number));
                        this.render();
                    }
                    break;
                case "filter":
                    this.render();
                    break;
            }
        });

        this.$container.on("scroll", this.onScroll);
    }
    private onScroll = () => {
        const { scrollHeight, offsetHeight, scrollTop } = this
            .container as HTMLElement;
        // safari bounce effect
        if (scrollTop <= 0) return;
        if (offsetHeight + scrollTop > scrollHeight) return;

        let isAtBottom = false;
        if (scrollHeight === offsetHeight) {
            isAtBottom = true;
        } else if (scrollTop === scrollHeight - offsetHeight) {
            isAtBottom = true;
        }
        this.isAtBottom = isAtBottom;

        const lastScrollTop = this.lastScrollTop;
        const lastTimestamp = this.lastTimestamp;

        const timestamp = now();
        const duration = timestamp - lastTimestamp;
        const distance = scrollTop - lastScrollTop;
        const speed = Math.abs(distance / duration);
        let speedTolerance = speed * this.speedToleranceFactor;
        if (duration > 1000) {
            speedTolerance = 1000;
        }
        if (speedTolerance > this.maxSpeedTolerance) {
            speedTolerance = this.maxSpeedTolerance;
        }
        if (speedTolerance < this.minSpeedTolerance) {
            speedTolerance = this.minSpeedTolerance;
        }
        this.lastScrollTop = scrollTop;
        this.lastTimestamp = timestamp;

        let topTolerance = 0;
        let bottomTolerance = 0;
        if (lastScrollTop < scrollTop) {
            topTolerance = this.minSpeedTolerance;
            bottomTolerance = speedTolerance;
        } else {
            topTolerance = speedTolerance;
            bottomTolerance = this.minSpeedTolerance;
        }

        if (
            this.topSpaceHeight < scrollTop - topTolerance &&
            this.topSpaceHeight + this.el.offsetHeight >
                scrollTop + offsetHeight + bottomTolerance
        ) {
            return;
        }

        this.renderViewport({
            topTolerance: topTolerance * 2,
            bottomTolerance: bottomTolerance * 2,
        });
    };
    private _renderViewport({
        topTolerance = 500,
        bottomTolerance = 500,
    } = {}) {
        const { el, container } = this;
        if (isHidden(container)) return;
        const { scrollTop, offsetHeight } = container as HTMLElement;
        const containerWidth = container.getBoundingClientRect().width;
        const top = scrollTop - topTolerance;
        const bottom = scrollTop + offsetHeight + bottomTolerance;

        const { displayLogs } = this;

        let topSpaceHeight = 0;
        let bottomSpaceHeight = 0;
        let currentHeight = 0;

        const len = displayLogs.length;

        const { fakeEl } = this;
        const fakeFrag = document.createDocumentFragment();
        const logs = [];
        for (let i = 0; i < len; i++) {
            const log = displayLogs[i];
            const { width, height } = log;
            if (height === 0 || width !== containerWidth) {
                fakeFrag.appendChild(log.container);
                logs.push(log);
            }
        }
        if (logs.length > 0) {
            fakeEl.appendChild(fakeFrag);
            for (let i = 0, len = logs.length; i < len; i++) {
                logs[i].updateSize();
            }
            fakeEl.innerHTML = "";
        }

        const frag = document.createDocumentFragment();
        for (let i = 0; i < len; i++) {
            const log = displayLogs[i];
            const { container, height } = log;

            if (currentHeight > bottom) {
                bottomSpaceHeight += height;
            } else if (currentHeight + height > top) {
                frag.appendChild(container);
            } else if (currentHeight < top) {
                topSpaceHeight += height;
            }

            currentHeight += height;
        }

        this.updateSpace(currentHeight);
        this.updateTopSpace(topSpaceHeight);
        this.updateBottomSpace(bottomSpaceHeight);

        while (el.firstChild) {
            if (el.lastChild) {
                el.removeChild(el.lastChild);
            }
        }
        el.appendChild(frag);

        const { scrollHeight } = container;
        if (this.isAtBottom && scrollTop <= scrollHeight - offsetHeight) {
            container.scrollTop = 10000000;
        }
    }
    private displayLogs: Log[] = [];
    private lastLog?: Log;
    private isAtBottom = true;
    private groupStack = new Stack();
    private initTpl() {
        this.$container.html(
            htmlC(stripIndent`
  <div class="logs-space">
    <div class="fake-logs"></div>
    <div class="logs"></div>
  </div>
`)
        );
    }
}
