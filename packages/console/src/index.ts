import types from "licia/types";
import { IComponentOptions } from "./util/Component";
/** IOptions */
export interface IOptions extends IComponentOptions {
    /** Max log number, zero means infinite. */
    maxNum?: number;
    /** Asynchronous rendering. */
    asyncRender?: boolean;
    /** Show time and from. */
    showHeader?: boolean;
    /** Access getter value. */
    accessGetter?: boolean;
    /** Show unenumerable properties. */
    unenumerable?: boolean;
    /** Lazy evaluation for objects. */
    lazyEvaluation?: boolean;
    /** Log filter. */
    filter?: string | RegExp | types.AnyFn;
}
import "./icon.css";
import "./luna-object-viewer.css";
export { ConsoleView } from "./ConsoleView";
export { Console } from "./Console";
