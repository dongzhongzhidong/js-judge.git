# @forsee/console

这是一个浏览器 Console 拦截查看的插件，修改自 luna-console ，分离了视图层和 Console ，可以通过特定的沟通渠道将 Console 的信息发送给 视图层，从而使得在 Worker 中也能够运行。

## 正常线程

```js
import { Console, ConsoleView } from "@forsee/console";
const el = document.getElementById("app");
const view = new ConsoleView(el, {
    maxNum: 1000,
});
const console = new Console();

console.init(
    (type, args) => {
        return view.insertSync(type, args && JSON.parse(args));
    },
    (silent) => view.clear(silent)
);

// ... 使用 console
```

## Worker 线程

```js
// 主线程
import { ConsoleView } from "@forsee/console";
import { wrap, proxy } from "comlink";

const el = document.getElementById("app");
const view = new ConsoleView(el, {
    maxNum: 1000,
});
const api = wrap(
    new Worker(
        // Worker URL
        "./test/worker.js",
        { type: "module" }
    )
);
await api.console.init(
    proxy((type, args) => {
        return view.insertSync(type, args && JSON.parse(args));
    }),
    proxy((silent) => view.clear(silent)),
    "worker"
);
api.start();
```

```js
// Worker 线程
import { expose } from "https://unpkg.com/comlink/dist/esm/comlink.mjs";
import { Console } from "@forsee/console";
const console = new Console();
expose({
    console,
    start() {
        // 使用 console
    },
});
```

## 其他参数调整

```js
// 正则表达式筛选，可以通过 文本转 RegExp 实现搜索功能
view.setOption("filter", /\d+/);

// 设置类别筛选功能
view.setOption("filter", "log");

// 自动清空控制台
console.clear();
```
