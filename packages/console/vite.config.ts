import { defineConfig } from "vite";
import solidPlugin from "vite-plugin-solid";
import visualizer from "rollup-plugin-visualizer";

export default defineConfig((config) => {
    const { mode } = config;
    console.log(mode);
    return {
        plugins: [
            solidPlugin(),

            mode === "analyze" &&
                (visualizer({
                    open: true,
                    filename: "visualizer/stat.html",
                }) as any),
        ],

        server: {
            port: 3000,
        },
        build: {
            lib: {
                entry: "./src/index.ts",
                formats: ["es"],
                fileName: "index",
            },
        },
    };
});
