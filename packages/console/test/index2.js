import { Console } from "../src/index";
import { ConsoleView } from "../src/index";

const el = document.getElementById("app");
const view = new ConsoleView(el, {
    maxNum: 1000,
});

const console = new Console();
console.init(
    (type, args) => {
        return view.insertSync(type, args);
    },
    (silent) => view.clear(silent)
);
function logMessage() {
    console.log("log");
    console.warn("warn");
    console.error(Error("test"));
    console.info("info");
    console.debug("debug");
    // worker 中没有 dom
    // console.dir(document.createElement("div"));
    console.time("test");
    console.timeEnd("test");
    console.count("luna");
    console.count("luna");
    console.assert(true, "assert msg");
    var site1 = { name: "Runoob", site: "www.runoob.com" };
    var site2 = { name: "Google", site: "www.google.com" };
    var site3 = { name: "Taobao", site: "www.taobao.com" };
    console.table([site1, site2, site3], ["site"]);
    console.log("%c Oh my heavens!", "background: #222; color: #bada55");
    console.log("This is the outer level");
    console.group();
    console.log("Level 2");
    console.group();
    console.log("Level 3");
    console.warn("More of level 3");
    console.groupEnd();
    console.log("Back to level 2");
    console.groupEnd();
    console.log("Back to the outer level");
    // worker 中没有原生元素
    console.log(navigator);
    console.log(location);
    console.log(performance);
    var arr = [];
    for (var i = 0; i < 10000; i++) arr.push(i);
    console.log(arr);
}

logMessage();

const code = "1 + 2";
console.evaluate(code);
