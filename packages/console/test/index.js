import { ConsoleView } from "../dist/index.js";
import { wrap, proxy } from "https://unpkg.com/comlink/dist/esm/comlink.mjs";

const el = document.getElementById("app");
const view = new ConsoleView(el, {
    maxNum: 1000,
});
const api = wrap(new Worker("./test/worker.js", { type: "module" }));
await api.console.init(
    proxy((type, args) => {
        return view.insertSync(type, args && JSON.parse(args));
    }),
    proxy((silent) => view.clear(silent)),
    "worker"
);
await api.start();
console.log(view);

// 正则表达式筛选，可以通过 文本转 RegExp 实现搜索功能
// view.setOption("filter", /\d+/);

// 设置类别筛选功能
// view.setOption("filter", "log");

// 清空控制台
// api.console.clear()
