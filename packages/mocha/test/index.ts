import { Judge } from "../src/Judge";
const judge = new Judge();

judge
    .setupBDD(() => {
        return import("./index.test");
    })
    .then(() => {
        judge.run((i) => {
            console.log(i, "完成啦");
        });
        judge.reporter.addReceiver((event) => {
            console.log(event);
        });
    });

console.log(judge);
