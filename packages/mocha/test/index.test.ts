import { use, expect } from "chai";

import { describe, it } from "../src/exports";
describe("测试", () => {
    it("第一次测试", () => {
        expect(2).eq(1);
    });
    it("第二次测试", () => {
        expect(1 + 1).lessThanOrEqual(2);
    });
});
