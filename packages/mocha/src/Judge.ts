import { Context } from "./context";
import { bddInterface } from "./interfaces/bdd";
import { Suite } from "./suite";
import { constants } from "./constants";
import { createStatsCollector } from "./stats-collector";
import {
    createMochaInstanceAlreadyDisposedError,
    createMochaInstanceAlreadyRunningError,
} from "./errors";
import { castArray, defineConstants, noop } from "./utils";
import Runner from "./runner";
import { JSONCollection } from "./Reporter/json";
import { Base } from "./Reporter/base";
import { injectContext } from "./exports";
import { MochaOptions } from "mocha";
import debug from "debug";
var mochaStates = defineConstants({
    /**
     * Initial state of the mocha instance
     * @private
     */
    INIT: "init",
    /**
     * Mocha instance is running tests
     * @private
     */
    RUNNING: "running",
    /**
     * Mocha instance is done running tests and references to test functions and hooks are cleaned.
     * You can reset this state by unloading the test files.
     * @private
     */
    REFERENCES_CLEANED: "referencesCleaned",
    /**
     * Mocha instance is disposed and can no longer be used.
     * @private
     */
    DISPOSED: "disposed",
});
type MochaGlobalFixture = () => void;
export class Judge {
    suite: Suite;
    reporter: Base;
    options: MochaOptions & {
        enableGlobalSetup: boolean;
        enableGlobalTeardown: boolean;
        globalSetup: MochaGlobalFixture[];
        globalTeardown: MochaGlobalFixture[];
    } = {
        delay: false,
        forbidPending: false,
        forbidOnly: false,
        grep: "",
        invert: true,
        enableGlobalSetup: false,
        enableGlobalTeardown: false,
        globalSetup: [],
        globalTeardown: [],
    };
    _previousRunner: any;
    _runnerClass: any;
    _cleanReferencesAfterRun: any;
    _reporter: any;
    files: any;
    async setupBDD(
        runtime: (context: any, judge: Judge) => Promise<any> | any
    ) {
        const suite = new Suite("", new Context(), true);
        this.suite = suite;
        bddInterface(this.suite);
        const global = {};
        return new Promise((res) => {
            this.suite.on(constants.EVENT_FILE_PRE_REQUIRE, function (context) {
                suite.emit(constants.EVENT_FILE_REQUIRE, global, runtime, self);
                injectContext(context);

                res(runtime(context, this));
            });
            suite.emit(constants.EVENT_FILE_PRE_REQUIRE, global, runtime, this);
        }).then(() => {
            suite.emit(
                constants.EVENT_FILE_POST_REQUIRE,
                global,
                runtime,
                self
            );
        });
    }

    /**
     * @summary
     * Sets `grep` filter used to select specific tests for execution.
     *
     * @description
     * If `re` is a regexp-like string, it will be converted to regexp.
     * The regexp is tested against the full title of each test (i.e., the
     * name of the test preceded by titles of each its ancestral suites).
     * As such, using an <em>exact-match</em> fixed pattern against the
     * test name itself will not yield any matches.
     * <br>
     * <strong>Previous filter value will be overwritten on each call!</strong>
     *
   
     * @example
     *
     * // Select tests whose full title contains `"match"`, ignoring case
     * mocha.grep(/match/i);
     * @example
     *
     * // Same as above but with regexp-like string argument
     * mocha.grep('/match/i');
     * @example
     *
     * // ## Anti-example
     * // Given embedded test `it('only-this-test')`...
     * mocha.grep('/^only-this-test$/');    // NO! Use `.only()` to do this!
     */
    grep(re: string | RegExp): this {
        if (!(re instanceof RegExp)) {
            // extract args if it's regex-like, i.e: [string, pattern, flag]
            var arg = re.match(/^\/(.*)\/([gimy]{0,4})$|.*/);
            this.options.grep = new RegExp(arg[1] || arg[0], arg[2]);
        } else {
            this.options.grep = re;
        }
        return this;
    }
    _guardRunningStateTransition = function () {
        if (this._state === mochaStates.RUNNING) {
            throw createMochaInstanceAlreadyRunningError(
                "Mocha instance is currently running tests, cannot start a next test run until this one is done",
                this
            );
        }
        if (
            this._state === mochaStates.DISPOSED ||
            this._state === mochaStates.REFERENCES_CLEANED
        ) {
            throw createMochaInstanceAlreadyDisposedError(
                "Mocha instance is already disposed, cannot start a new test run. Please create a new mocha instance. Be sure to set disable `cleanReferencesAfterRun` when you want to reuse the same mocha instance for multiple test runs.",
                this._cleanReferencesAfterRun,
                this
            );
        }
    };
    _state: any;
    run(fn: (error: 0 | 1) => void) {
        this._guardRunningStateTransition();
        this._state = mochaStates.RUNNING;
        if (this._previousRunner) {
            this._previousRunner.dispose();
            this.suite.reset();
        }

        var suite = this.suite;
        var options = this.options;
        // options.files = this.files;
        const runner = new Runner(suite, {
            cleanReferencesAfterRun: this._cleanReferencesAfterRun,
            // delay: options.delay,
            // dryRun: options.dryRun,
            // failZero: options.failZero,
            ...options,
        });
        createStatsCollector(runner);
        const reporter = new JSONCollection(runner, options);
        this.reporter = reporter;
        Object.assign(runner, {
            ...options,
        });
        // runner.checkLeaks = options.checkLeaks === true;

        // if (options.grep) {
        //     runner.grep(options.grep, options.invert);
        // }
        // if (options.global) {
        //     runner.globals(options.global);
        // }
        // if (options.color !== undefined) {
        //     exports.reporters.Base.useColors = options.color;
        // }
        // exports.reporters.Base.inlineDiffs = options.inlineDiffs;
        // exports.reporters.Base.hideDiff = !options.diff;

        const done = (failures) => {
            this._previousRunner = runner;
            this._state = this._cleanReferencesAfterRun
                ? mochaStates.REFERENCES_CLEANED
                : mochaStates.INIT;
            fn = fn || noop;
            if (typeof reporter.done === "function") {
                reporter.done(failures, fn);
            } else {
                fn(failures);
            }
        };

        const runAsync = async (runner) => {
            const context =
                this.options.enableGlobalSetup &&
                this.options.globalSetup.length
                    ? await this.runGlobalSetup(runner)
                    : {};
            const failureCount = await runner.runAsync({
                files: this.files,
                options,
            });
            if (
                this.options.enableGlobalTeardown &&
                this.options.globalTeardown.length
            ) {
                await this.runGlobalTeardown(runner);
            }
            return failureCount;
        };

        // no "catch" here is intentional. errors coming out of
        // Runner#run are considered uncaught/unhandled and caught
        // by the `process` event listeners.
        // also: returning anything other than `runner` would be a breaking
        // change
        runAsync(runner).then(done);

        return runner;
    }
    isWorker: true;
    /**
     * Configures one or more global setup fixtures.
     *
     * If given no parameters, _unsets_ any previously-set fixtures.
     */
    globalSetup(
        setupFns: MochaGlobalFixture | MochaGlobalFixture[] = []
    ): this {
        setupFns = castArray(setupFns);
        this.options.globalSetup = setupFns;
        debug(`configured ${setupFns.length} global setup functions`);
        return this;
    }

    /**
     * Configures one or more global teardown fixtures.
     *
     * If given no parameters, _unsets_ any previously-set fixtures.
     * @chainable
     * @public
     * @param {MochaGlobalFixture|MochaGlobalFixture[]} [teardownFns] - Global teardown fixture(s)
     * @returns {Mocha}
     */
    globalTeardown(
        teardownFns: MochaGlobalFixture | MochaGlobalFixture[] = []
    ): this {
        teardownFns = castArray(teardownFns);
        this.options.globalTeardown = teardownFns;
        debug(`configured ${teardownFns.length} global teardown functions`);
        return this;
    }

    /**
     * Run any global setup fixtures sequentially, if any.
     *
     * This is _automatically called_ by {@link Mocha#run} _unless_ the `runGlobalSetup` option is `false`; see {@link Mocha#enableGlobalSetup}.
     *
     * The context object this function resolves with should be consumed by {@link Mocha#runGlobalTeardown}.
     * @param {object} [context] - Context object if already have one
     * @public
     * @returns {Promise<object>} Context object
     */
    async runGlobalSetup(context: object = {}): Promise<object> {
        const { globalSetup } = this.options;
        if (globalSetup && globalSetup.length) {
            debug("run(): global setup starting");
            await this._runGlobalFixtures(globalSetup, context);
            debug("run(): global setup complete");
        }
        return context;
    }

    /**
     * Run any global teardown fixtures sequentially, if any.
     *
     * This is _automatically called_ by {@link Mocha#run} _unless_ the `runGlobalTeardown` option is `false`; see {@link Mocha#enableGlobalTeardown}.
     *
     * Should be called with context object returned by {@link Mocha#runGlobalSetup}, if applicable.
     * @param {object} [context] - Context object if already have one
     * @public
     * @returns {Promise<object>} Context object
     */
    async runGlobalTeardown(context: object = {}): Promise<object> {
        const { globalTeardown } = this.options;
        if (globalTeardown && globalTeardown.length) {
            debug("run(): global teardown starting");
            await this._runGlobalFixtures(globalTeardown, context);
        }
        debug("run(): global teardown complete");
        return context;
    }

    /**
     * Run global fixtures sequentially with context `context`
     * @private
     * @param {MochaGlobalFixture[]} [fixtureFns] - Fixtures to run
     * @param {object} [context] - context object
     * @returns {Promise<object>} context object
     */
    async _runGlobalFixtures<T extends {}>(
        fixtureFns: MochaGlobalFixture[] = [],
        context: T
    ): Promise<T> {
        for await (const fixtureFn of fixtureFns) {
            await fixtureFn.call(context);
        }
        return context;
    }
}
