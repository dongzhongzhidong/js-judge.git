import { Base } from "./base";
import Runner, { constants } from "../runner";
var EVENT_TEST_PASS = constants.EVENT_TEST_PASS;
var EVENT_TEST_FAIL = constants.EVENT_TEST_FAIL;
var EVENT_RUN_BEGIN = constants.EVENT_RUN_BEGIN;
var EVENT_RUN_END = constants.EVENT_RUN_END;

export class JSONCollection extends Base {
    result = [];
    running;
    done?: Function;
    constructor(runner: Runner, options) {
        super(runner, options);

        var self = this;
        var total = runner.total;
        this.running = new Promise((res) => {
            runner.once(EVENT_RUN_BEGIN, () => {
                this.writeEvent(["start", { total }]);
            });

            runner.on(EVENT_TEST_PASS, (test) => {
                this.writeEvent(["pass", clean(test)]);
            });

            runner.on(EVENT_TEST_FAIL, (test, err) => {
                test = clean(test);
                test.err = err.message;
                test.stack = err.stack || null;
                this.writeEvent(["fail", test]);
            });

            runner.once(EVENT_RUN_END, () => {
                this.writeEvent(["end", self.stats]);
                res(this.result);
            });
        });
    }

    writeEvent(event) {
        this.receiver && this.receiver(event, this.result);
        this.result.push(event);
    }
}

/**
 * Returns an object literal representation of `test`
 * free of cyclic properties, etc.
 *
 * @private
 * @param {Test} test - Instance used as data source.
 * @return {Object} object containing pared-down test instance data
 */
function clean(test) {
    return {
        title: test.title,
        fullTitle: test.fullTitle(),
        file: test.file,
        duration: test.duration,
        currentRetry: test.currentRetry(),
        speed: test.speed,
    };
}
