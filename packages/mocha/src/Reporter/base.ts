/**
 * Module dependencies.
 */
import * as diff from "diff";
import milliseconds from "ms";
import { isString, stringify } from "../utils";

import _symbols from "log-symbols";
import Runner, { constants } from "../runner";
import { Error } from "../Error";
import Test from "../test";
import { MochaOptions } from "mocha";
var EVENT_TEST_PASS = constants.EVENT_TEST_PASS;
var EVENT_TEST_FAIL = constants.EVENT_TEST_FAIL;

function getBrowserWindowSize() {
    if ("innerHeight" in globalThis) {
        return [globalThis.innerHeight, globalThis.innerWidth];
    }
    // In a Web Worker, the DOM Window is not available.
    return [640, 480];
}

/**
 * Check if both stdio streams are associated with a tty.
 */

var isatty = true;

/**
 * Save log references to avoid tests interfering (see GH-3604).
 */
var consoleLog = console.log;

/**
 * Enable coloring by default, except in the browser interface.
 */

export const useColors = false;

/**
 * Inline diffs instead of +/-
 */

export const inlineDiffs = false;

/**
 * Truncate diffs longer than this value to avoid slow performance
 */
export let maxDiffSize = 8192;

/**
 * Default color map.
 */

export const colors = {
    pass: 90,
    fail: 31,
    "bright pass": 92,
    "bright fail": 91,
    "bright yellow": 93,
    pending: 36,
    suite: 0,
    "error title": 0,
    "error message": 31,
    "error stack": 90,
    checkmark: 32,
    fast: 90,
    medium: 33,
    slow: 31,
    green: 32,
    light: 90,
    "diff gutter": 90,
    "diff added": 32,
    "diff removed": 31,
    "diff added inline": "30;42",
    "diff removed inline": "30;41",
};

/**
 * Default symbol map.
 */

export const symbols = {
    ok: _symbols.success,
    err: _symbols.error,
    dot: ".",
    comma: ",",
    bang: "!",
};

/**
 * Color `str` with the given `type`,
 * allowing colors to be disabled,
 * as well as user-defined color
 * schemes.
 *
 * @private
 * @param {string} type
 * @param {string} str
 * @return {string}
 */
var wrapcolor = (type: string, str: string): string => {
    return str.toString();
};

/**
 * Expose term window size, with some defaults for when stderr is not a tty.
 */

export const window = {
    width: getBrowserWindowSize()[1] || 75,
};

/**
 * Expose some basic cursor interactions that are common among reporters.
 */

export const cursor = {
    hide: function () {
        isatty && process.stdout.write("\u001b[?25l");
    },

    show: function () {
        isatty && process.stdout.write("\u001b[?25h");
    },

    deleteLine: function () {
        isatty && process.stdout.write("\u001b[2K");
    },

    beginningOfLine: function () {
        isatty && process.stdout.write("\u001b[0G");
    },

    CR: function () {
        if (isatty) {
            cursor.deleteLine();
            cursor.beginningOfLine();
        } else {
            process.stdout.write("\r");
        }
    },
};

export const showDiff = function (err: any) {
    return (
        err &&
        err.showDiff !== false &&
        sameType(err.actual, err.expected) &&
        err.expected !== undefined
    );
};

function stringifyDiffObjs(err: { actual: any; expected: any }) {
    if (!isString(err.actual) || !isString(err.expected)) {
        err.actual = stringify(err.actual);
        err.expected = stringify(err.expected);
    }
}

/**
 * Returns a diff between 2 strings with coloured ANSI output.
 *
 * @description
 * The diff will be either inline or unified dependent on the value
 * of `Base.inlineDiff`.
 *
 * @param {string} actual
 * @param {string} expected
 * @return {string} Diff
 */

export const generateDiff = (actual: string, expected: string): string => {
    try {
        var maxLen = maxDiffSize;
        var skipped = 0;
        if (maxLen > 0) {
            skipped = Math.max(
                actual.length - maxLen,
                expected.length - maxLen
            );
            actual = actual.slice(0, maxLen);
            expected = expected.slice(0, maxLen);
        }
        let result = inlineDiffs
            ? inlineDiff(actual, expected)
            : unifiedDiff(actual, expected);
        if (skipped > 0) {
            result = `${result}\n      [mocha] output truncated to ${maxLen} characters, see "maxDiffSize" reporter-option\n`;
        }
        return result;
    } catch (err) {
        var msg =
            "\n      " +
            wrapcolor("diff added", "+ expected") +
            " " +
            wrapcolor(
                "diff removed",
                "- actual:  failed to generate Mocha diff"
            ) +
            "\n";
        return msg;
    }
};

/**
 * Outputs the given `failures` as a list.
 *
 * @public
 * @memberof Mocha.reporters.Base
 * @variation 1
 * @param {Object[]} failures - Each is Test instance with corresponding
 *     Error property
 */
export const list = function (failures: Test[]) {
    var multipleErr: any[], multipleTest: Test;
    Base.consoleLog();
    failures.forEach(function (test, i) {
        // format
        var fmt =
            wrapcolor("error title", "  %s) %s:\n") +
            wrapcolor("error message", "     %s") +
            wrapcolor("error stack", "\n%s\n");

        // msg
        var msg: string;
        var err: {
            inspect: () => string;
            message: string;
            stack: any;
            uncaught: any;
            actual: string;
            expected: string;
        };
        if (test.err && test.err.multiple) {
            if (multipleTest !== test) {
                multipleTest = test;
                multipleErr = [test.err].concat(test.err.multiple);
            }
            err = multipleErr.shift();
        } else {
            err = test.err;
        }
        var message: string;
        if (typeof err.inspect === "function") {
            message = err.inspect() + "";
        } else if (err.message && typeof err.message.toString === "function") {
            message = err.message + "";
        } else {
            message = "";
        }
        var stack = err.stack || message;
        var index = message ? stack.indexOf(message) : -1;

        if (index === -1) {
            msg = message;
        } else {
            index += message.length;
            msg = stack.slice(0, index);
            // remove msg from stack
            stack = stack.slice(index + 1);
        }

        // uncaught
        if (err.uncaught) {
            msg = "Uncaught " + msg;
        }
        // explicitly show diff
        if (
            // !hideDiff &&
            showDiff(err)
        ) {
            stringifyDiffObjs(err);
            fmt =
                wrapcolor("error title", "  %s) %s:\n%s") +
                wrapcolor("error stack", "\n%s\n");
            var match = message.match(/^([^:]+): expected/);
            msg =
                "\n      " + wrapcolor("error message", match ? match[1] : msg);

            msg += generateDiff(err.actual, err.expected);
        }

        // indent stack trace
        stack = stack.replace(/^/gm, "  ");

        // indented test title
        var testTitle = "";
        test.titlePath().forEach(function (str, index) {
            if (index !== 0) {
                testTitle += "\n     ";
            }
            for (var i = 0; i < index; i++) {
                testTitle += "  ";
            }
            testTitle += str;
        });

        Base.consoleLog(fmt, i + 1, testTitle, msg, stack);
    });
};

/**
 * Constructs a new `Base` reporter instance.
 *
 * @description
 * All other reporters generally inherit from this reporter.
 */
export class Base implements Mocha.reporters.Base {
    static consoleLog = consoleLog;
    static abstract = true;
    failures: any[];
    options: any;
    runner: any;
    stats: any;

    static list = list;
    constructor(runner: Runner, options: MochaOptions) {
        var failures = (this.failures = []);

        if (!runner) {
            throw new TypeError("Missing runner argument");
        }
        this.options = options || {};
        this.runner = runner;
        this.stats = runner.stats; // assigned so Reporters keep a closer reference

        var maxDiffSizeOpt =
            this.options.reporterOption &&
            this.options.reporterOption.maxDiffSize;
        if (maxDiffSizeOpt !== undefined && !isNaN(Number(maxDiffSizeOpt))) {
            maxDiffSize = Number(maxDiffSizeOpt);
        }

        runner.on(EVENT_TEST_PASS, function (test) {
            if (test.duration > test.slow()) {
                test.speed = "slow";
            } else if (test.duration > test.slow() / 2) {
                test.speed = "medium";
            } else {
                test.speed = "fast";
            }
        });

        runner.on(EVENT_TEST_FAIL, function (test, err) {
            if (showDiff(err)) {
                stringifyDiffObjs(err);
            }
            // more than one error per test
            if (test.err && err instanceof Error) {
                test.err.multiple = (test.err.multiple || []).concat(err);
            } else {
                test.err = err;
            }
            failures.push(test);
        });
    }
    receiver?: Function;
    addReceiver(func: Function) {
        this.receiver = func;
    }
    /**
     * Outputs common epilogue used by many of the bundled reporters.
     *
     * @public
     * @memberof Mocha.reporters
     */
    epilogue = function () {
        var stats = this.stats;
        var fmt: string;

        Base.consoleLog();

        // passes
        fmt =
            wrapcolor("bright pass", " ") +
            wrapcolor("green", " %d passing") +
            wrapcolor("light", " (%s)");

        Base.consoleLog(fmt, stats.passes || 0, milliseconds(stats.duration));

        // pending
        if (stats.pending) {
            fmt =
                wrapcolor("pending", " ") + wrapcolor("pending", " %d pending");

            Base.consoleLog(fmt, stats.pending);
        }

        // failures
        if (stats.failures) {
            fmt = wrapcolor("fail", "  %d failing");

            Base.consoleLog(fmt, stats.failures);

            Base.list(this.failures);
            Base.consoleLog();
        }

        Base.consoleLog();
    };
}
/**
 * Pads the given `str` to `len`.
 *
 */
function pad(_str: number | number, len: number): string {
    let str = String(_str);
    return Array(len - str.length + 1).join(" ") + str;
}

/**
 * Returns inline diff between 2 strings with coloured ANSI output.
 *
 * @private
 * @param {String} actual
 * @param {String} expected
 * @return {string} Diff
 */
function inlineDiff(actual: string, expected: string): string {
    var msg = errorDiff(actual, expected);

    // linenos
    var lines = msg.split("\n");
    if (lines.length > 4) {
        var width = String(lines.length).length;
        msg = lines
            .map(function (str, i) {
                return pad(++i, width) + " |" + " " + str;
            })
            .join("\n");
    }

    // legend
    msg =
        "\n" +
        wrapcolor("diff removed inline", "actual") +
        " " +
        wrapcolor("diff added inline", "expected") +
        "\n\n" +
        msg +
        "\n";

    // indent
    msg = msg.replace(/^/gm, "      ");
    return msg;
}

/**
 * Returns unified diff between two strings with coloured ANSI output.
 *
 * @private
 * @param {String} actual
 * @param {String} expected
 * @return {string} The diff.
 */
function unifiedDiff(actual: string, expected: string): string {
    var indent = "      ";
    function cleanUp(line: string) {
        if (line[0] === "+") {
            return indent + colorLines("diff added", line);
        }
        if (line[0] === "-") {
            return indent + colorLines("diff removed", line);
        }
        if (line.match(/@@/)) {
            return "--";
        }
        if (line.match(/\\ No newline/)) {
            return null;
        }
        return indent + line;
    }
    function notBlank(line: any) {
        return typeof line !== "undefined" && line !== null;
    }
    var msg = diff.createPatch("string", actual, expected);
    var lines = msg.split("\n").splice(5);
    return (
        "\n      " +
        colorLines("diff added", "+ expected") +
        " " +
        colorLines("diff removed", "- actual") +
        "\n\n" +
        lines.map(cleanUp).filter(notBlank).join("\n")
    );
}

/**
 * Returns character diff for `err`.
 *
 * @private
 * @param {String} actual
 * @param {String} expected
 * @return {string} the diff
 */
function errorDiff(actual: string, expected: string): string {
    return diff
        .diffWordsWithSpace(actual, expected)
        .map(function (str) {
            if (str.added) {
                return colorLines("diff added inline", str.value);
            }
            if (str.removed) {
                return colorLines("diff removed inline", str.value);
            }
            return str.value;
        })
        .join("");
}

/**
 * Colors lines for `str`, using the color `name`.
 *
 * @private
 * @param {string} name
 * @param {string} str
 * @return {string}
 */
function colorLines(name: string, str: string): string {
    return str
        .split("\n")
        .map(function (str) {
            return wrapcolor(name, str);
        })
        .join("\n");
}

/**
 * Object#toString reference.
 */
var objToString = Object.prototype.toString;

/**
 * Checks that a / b have the same type.
 *
 * @private
 * @param {Object} a
 * @param {Object} b
 * @return {boolean}
 */
function sameType(a: object, b: object): boolean {
    return objToString.call(a) === objToString.call(b);
}
