"use strict";

/**
 * Initialize a new `Pending` error with the given message.
 */
export function Pending(message: string) {
    this.message = message;
}
