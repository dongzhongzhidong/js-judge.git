import { defineConstants } from "./utils";
export const constants = defineConstants(
    /**
     * {@link Suite}-related constants.
     * @public
     * @memberof Suite
     * @alias constants
     * @readonly
     * @static
     * @enum {string}
     */
    {
        /**
         * Event emitted after a test file has been loaded. Not emitted in browser.
         */
        EVENT_FILE_POST_REQUIRE: "post-require",
        /**
         * Event emitted before a test file has been loaded. In browser, this is emitted once an interface has been selected.
         */
        EVENT_FILE_PRE_REQUIRE: "pre-require",
        /**
         * Event emitted immediately after a test file has been loaded. Not emitted in browser.
         */
        EVENT_FILE_REQUIRE: "require",
        /**
         * Event emitted when `global.run()` is called (use with `delay` option).
         */
        EVENT_ROOT_SUITE_RUN: "run",

        /**
         * Namespace for collection of a `Suite`'s "after all" hooks.
         */
        HOOK_TYPE_AFTER_ALL: "afterAll",
        /**
         * Namespace for collection of a `Suite`'s "after each" hooks.
         */
        HOOK_TYPE_AFTER_EACH: "afterEach",
        /**
         * Namespace for collection of a `Suite`'s "before all" hooks.
         */
        HOOK_TYPE_BEFORE_ALL: "beforeAll",
        /**
         * Namespace for collection of a `Suite`'s "before each" hooks.
         */
        HOOK_TYPE_BEFORE_EACH: "beforeEach",

        /**
         * Emitted after a child `Suite` has been added to a `Suite`.
         */
        EVENT_SUITE_ADD_SUITE: "suite",
        /**
         * Emitted after an "after all" `Hook` has been added to a `Suite`.
         */
        EVENT_SUITE_ADD_HOOK_AFTER_ALL: "afterAll",
        /**
         * Emitted after an "after each" `Hook` has been added to a `Suite`.
         */
        EVENT_SUITE_ADD_HOOK_AFTER_EACH: "afterEach",
        /**
         * Emitted after an "before all" `Hook` has been added to a `Suite`.
         */
        EVENT_SUITE_ADD_HOOK_BEFORE_ALL: "beforeAll",
        /**
         * Emitted after an "before each" `Hook` has been added to a `Suite`.
         */
        EVENT_SUITE_ADD_HOOK_BEFORE_EACH: "beforeEach",
        /**
         * Emitted after a `Test` has been added to a `Suite`.
         */
        EVENT_SUITE_ADD_TEST: "test",
    }
);
