"use strict";

/**
 * Module dependencies.
 * @private
 */
import { EventEmitter } from "events";
import { constants } from "./constants";
import { Hook } from "./hook";
import {
    clamp,
    getMochaID,
    isString,
    uniqueID,
    MOCHA_ID_PROP_NAME,
} from "./utils";
import { createDebug } from "./debug";
const debug = createDebug("mocha:suite");
import milliseconds from "ms";
import { createInvalidArgumentTypeError } from "./errors";
import { Context } from "./context";
import Test from "./test";

/**
 * Expose `Suite`.
 */

export { Suite };
class Suite extends EventEmitter {
    [MOCHA_ID_PROP_NAME] = uniqueID();
    /**
     * Create a new `Suite` with the given `title` and parent `Suite`.
     */
    static create = function (parent: Suite, title: string) {
        var suite = new Suite(title, parent.ctx);
        suite.parent = parent;
        title = suite.fullTitle();
        parent.addSuite(suite);
        return suite;
    };
    ctx: any;
    parent: Suite;
    title: string;

    root: boolean;
    suites = [];
    tests = [];

    pending = false;
    _retries = -1;
    _beforeEach = [];
    _beforeAll = [];
    _afterEach = [];
    _afterAll = [];
    _timeout = 2000;
    _slow = "75";
    _bail = false;
    _onlyTests = [];
    _onlySuites = [];

    delayed: boolean;
    file: any;
    get id() {
        return getMochaID(this);
    }
    /**
     * Constructs a new `Suite` instance with the given `title`, `ctx`, and `isRoot`.
     *
     * @see {@link https://nodejs.org/api/events.html#events_class_eventemitter|EventEmitter}
     * @param {string} title - Suite title.
     * @param {Context} parentContext - Parent context instance.
     * @param {boolean} [isRoot=false] - Whether this is the root suite.
     */
    constructor(
        title: string,
        parentContext?: Context,
        isRoot: boolean = false
    ) {
        super();
        if (!isString(title)) {
            throw createInvalidArgumentTypeError(
                'Suite argument "title" must be a string. Received type "' +
                    typeof title +
                    '"',
                "title",
                "string"
            );
        }
        this.title = title;
        this.root = isRoot === true;
        function Context() {}
        Context.prototype = parentContext;
        this.ctx = new Context();

        this.reset();
    }

    /**
     * Resets the state initially or for a next run.
     */
    reset() {
        this.delayed = false;
        function doReset(thingToReset) {
            thingToReset.reset();
        }
        this.suites.forEach(doReset);
        this.tests.forEach(doReset);
        this._beforeEach.forEach(doReset);
        this._afterEach.forEach(doReset);
        this._beforeAll.forEach(doReset);
        this._afterAll.forEach(doReset);
    }

    /**
     * Return a clone of this `Suite`.
     *
     */
    clone() {
        var suite = new Suite(this.title);
        debug("clone");
        suite.ctx = this.ctx;
        suite.root = this.root;
        suite.timeout(this.timeout());
        suite.retries(this.retries());
        suite.slow(this.slow());
        suite.bail(this.bail());
        return suite;
    }

    /**
     * Set or get timeout `ms` or short-hand such as "2s".
     *
     */
    timeout(): number;
    timeout(ms: number | string): Suite;
    timeout(ms?: number | string): Suite | number {
        if (!arguments.length) {
            return this._timeout;
        }
        if (typeof ms === "string") {
            ms = milliseconds(ms);
        }

        // Clamp to range
        var INT_MAX = Math.pow(2, 31) - 1;
        var range = [0, INT_MAX];
        ms = clamp(ms, range);

        debug("timeout %d", ms);
        this._timeout = parseInt(ms as unknown as string, 10);
        return this;
    }

    /**
     * Set or get number of times to retry a failed test.
     */
    retries(): number;
    retries(n: number | string): Suite;
    retries(n?: number | string): Suite | number {
        if (!arguments.length) {
            return this._retries;
        }
        debug("retries %d", n);
        this._retries = parseInt(n as any as string, 10) || 0;
        return this;
    }

    /**
     * Set or get slow `ms` or short-hand such as "2s".
     *
     * @private
     * @param {number} ms
     * @return {Suite|string} for chaining
     */
    slow(): string;
    slow(ms: number | string): Suite;
    slow(ms?: number | string): Suite | string {
        let mss: string | number = ms;
        if (!arguments.length) {
            return this._slow;
        }
        if (typeof mss === "string") {
            // 需要把它转换为标准的单位
            mss = milliseconds(mss);
        }
        debug("slow %d", mss);
        this._slow = mss.toString();
        return this;
    }

    /**
     * Set or get whether to bail after first error.
     *
     * @private
     */
    bail(): boolean;
    bail(bail: boolean): Suite;
    bail(bail?: boolean): Suite | boolean {
        if (!arguments.length) {
            return this._bail;
        }
        debug("bail %s", bail);
        this._bail = bail;
        return this;
    }

    /**
     * Check if this suite or its parent suite is marked as pending.
     *
     * @private
     */
    isPending() {
        return this.pending || (this.parent && this.parent.isPending());
    }

    /**
     * Generic hook-creator.
     * @private
     * @param {string} title - Title of hook
     * @param {Function} fn - Hook callback
     * @returns {Hook} A new hook
     */
    _createHook(title: string, fn: Mocha.Func): Hook {
        var hook = new Hook(title, fn);
        hook.parent = this;
        hook.timeout(this.timeout());
        hook.retries(this.retries());
        hook.slow(this.slow());
        hook.ctx = this.ctx;
        hook.file = this.file;
        return hook;
    }

    /**
     * Run `fn(test[, done])` before running tests.
     *
     * @private
     * @param {string} title
     * @param {Function} fn
     * @return {Suite} for chaining
     */
    beforeAll(title: string, fn: Mocha.Func): Suite {
        if (this.isPending()) {
            return this;
        }
        if (typeof title === "function") {
            fn = title;
            title = fn.name;
        }
        title = '"before all" hook' + (title ? ": " + title : "");

        var hook = this._createHook(title, fn);
        this._beforeAll.push(hook);
        this.emit(constants.EVENT_SUITE_ADD_HOOK_BEFORE_ALL, hook);
        return this;
    }

    /**
     * Run `fn(test[, done])` after running tests.
     *
     * @private
     * @param {string} title
     * @param {Function} fn
     * @return {Suite} for chaining
     */
    afterAll(title: string, fn: Mocha.Func): Suite {
        if (this.isPending()) {
            return this;
        }
        if (typeof title === "function") {
            fn = title;
            title = fn.name;
        }
        title = '"after all" hook' + (title ? ": " + title : "");

        var hook = this._createHook(title, fn);
        this._afterAll.push(hook);
        this.emit(constants.EVENT_SUITE_ADD_HOOK_AFTER_ALL, hook);
        return this;
    }

    /**
     * Run `fn(test[, done])` before each test case.
     *
     * @private
     * @param {string} title
     * @param {Function} fn
     * @return {Suite} for chaining
     */
    beforeEach(title: string, fn: Mocha.Func): Suite {
        if (this.isPending()) {
            return this;
        }
        if (typeof title === "function") {
            fn = title;
            title = fn.name;
        }
        title = '"before each" hook' + (title ? ": " + title : "");

        var hook = this._createHook(title, fn);
        this._beforeEach.push(hook);
        this.emit(constants.EVENT_SUITE_ADD_HOOK_BEFORE_EACH, hook);
        return this;
    }

    /**
     * Run `fn(test[, done])` after each test case.
     *
     * @private
     * @param {string} title
     * @param {Function} fn
     * @return {Suite} for chaining
     */
    afterEach(title: string, fn: Mocha.Func): Suite {
        if (this.isPending()) {
            return this;
        }
        if (typeof title === "function") {
            fn = title;
            title = fn.name;
        }
        title = '"after each" hook' + (title ? ": " + title : "");

        var hook = this._createHook(title, fn);
        this._afterEach.push(hook);
        this.emit(constants.EVENT_SUITE_ADD_HOOK_AFTER_EACH, hook);
        return this;
    }

    /**
     * Add a test `suite`.
     *
     * @private
     * @param {Suite} suite
     * @return {Suite} for chaining
     */
    addSuite(suite: Suite): Suite {
        suite.parent = this;
        suite.root = false;
        suite.timeout(this.timeout());
        suite.retries(this.retries());
        suite.slow(this.slow());
        suite.bail(this.bail());
        this.suites.push(suite);
        this.emit(constants.EVENT_SUITE_ADD_SUITE, suite);
        return this;
    }

    /**
     * Add a `test` to this suite.
     *
     * @private
     * @param {Test} test
     * @return {Suite} for chaining
     */
    addTest(test: Test): Suite {
        test.parent = this;
        test.timeout(this.timeout());
        test.retries(this.retries());
        test.slow(this.slow());
        test.ctx = this.ctx;
        this.tests.push(test);
        this.emit(constants.EVENT_SUITE_ADD_TEST, test);
        return this;
    }

    /**
     * Return the full title generated by recursively concatenating the parent's
     * full title.
     *
     * @memberof Suite
     * @public
     * @return {string}
     */
    fullTitle(): string {
        return this.titlePath().join(" ");
    }

    /**
     * Return the title path generated by recursively concatenating the parent's
     * title path.
     *
     * @memberof Suite
     * @public
     * @return {string[]}
     */
    titlePath(): string[] {
        var result = [];
        if (this.parent) {
            result = result.concat(this.parent.titlePath());
        }
        if (!this.root) {
            result.push(this.title);
        }
        return result;
    }

    /**
     * Return the total number of tests.
     *
     * @memberof Suite
     * @public
     * @return {number}
     */
    total(): number {
        return (
            this.suites.reduce(function (sum, suite) {
                return sum + suite.total();
            }, 0) + this.tests.length
        );
    }

    /**
     * Iterates through each suite recursively to find all tests. Applies a
     * function in the format `fn(test)`.
     *
     * @private
     * @param {Function} fn
     * @return {Suite}
     */
    eachTest(fn: any): Suite {
        this.tests.forEach(fn);
        this.suites.forEach(function (suite) {
            suite.eachTest(fn);
        });
        return this;
    }

    /**
     * This will run the root suite if we happen to be running in delayed mode.
     * @private
     */
    run() {
        if (this.root) {
            this.emit(constants.EVENT_ROOT_SUITE_RUN, undefined);
        }
    }

    /**
     * Determines whether a suite has an `only` test or suite as a descendant.
     *
     * @private
     * @returns {Boolean}
     */
    hasOnly(): boolean {
        return (
            this._onlyTests.length > 0 ||
            this._onlySuites.length > 0 ||
            this.suites.some(function (suite) {
                return suite.hasOnly();
            })
        );
    }

    /**
     * Filter suites based on `isOnly` logic.
     *
     * @private
     * @returns {Boolean}
     */
    filterOnly(): boolean {
        if (this._onlyTests.length) {
            // If the suite contains `only` tests, run those and ignore any nested suites.
            this.tests = this._onlyTests;
            this.suites = [];
        } else {
            // Otherwise, do not run any of the tests in this suite.
            this.tests = [];
            this._onlySuites.forEach(function (onlySuite) {
                // If there are other `only` tests/suites nested in the current `only` suite, then filter that `only` suite.
                // Otherwise, all of the tests on this `only` suite should be run, so don't filter it.
                if (onlySuite.hasOnly()) {
                    onlySuite.filterOnly();
                }
            });
            // Run the `only` suites, as well as any other suites that have `only` tests/suites as descendants.
            var onlySuites = this._onlySuites;
            this.suites = this.suites.filter(function (childSuite) {
                return (
                    onlySuites.indexOf(childSuite) !== -1 ||
                    childSuite.filterOnly()
                );
            });
        }
        // Keep the suite only if there is something to run
        return this.tests.length > 0 || this.suites.length > 0;
    }

    /**
     * Adds a suite to the list of subsuites marked `only`.
     *
     * @private
     * @param {Suite} suite
     */
    appendOnlySuite(suite: Suite) {
        this._onlySuites.push(suite);
    }

    /**
     * Marks a suite to be `only`.
     *
     * @private
     */
    markOnly() {
        this.parent && this.parent.appendOnlySuite(this);
    }

    /**
     * Adds a test to the list of tests marked `only`.
     *
     * @private
     * @param {Test} test
     */
    appendOnlyTest(test: Test) {
        this._onlyTests.push(test);
    }

    /**
     * Returns the array of hooks by hook name; see `HOOK_TYPE_*` constants.
     * @private
     */
    getHooks(name: string) {
        return this["_" + name];
    }

    /**
     * cleans all references from this suite and all child suites.
     */
    dispose() {
        this.suites.forEach(function (suite) {
            suite.dispose();
        });
        this.cleanReferences();
    }

    /**
     * Cleans up the references to all the deferred functions
     * (before/after/beforeEach/afterEach) and tests of a Suite.
     * These must be deleted otherwise a memory leak can happen,
     * as those functions may reference variables from closures,
     * thus those variables can never be garbage collected as long
     * as the deferred functions exist.
     */
    private cleanReferences() {
        function cleanArrReferences(arr) {
            for (var i = 0; i < arr.length; i++) {
                delete arr[i].fn;
            }
        }

        if (Array.isArray(this._beforeAll)) {
            cleanArrReferences(this._beforeAll);
        }

        if (Array.isArray(this._beforeEach)) {
            cleanArrReferences(this._beforeEach);
        }

        if (Array.isArray(this._afterAll)) {
            cleanArrReferences(this._afterAll);
        }

        if (Array.isArray(this._afterEach)) {
            cleanArrReferences(this._afterEach);
        }

        for (var i = 0; i < this.tests.length; i++) {
            delete this.tests[i].fn;
        }
    }

    /**
     * Returns an object suitable for IPC.
     * Functions are represented by keys beginning with `$$`.
     * @private
     * @returns {Object}
     */
    serialize(): object {
        return {
            _bail: this._bail,
            $$fullTitle: this.fullTitle(),
            $$isPending: Boolean(this.isPending()),
            root: this.root,
            title: this.title,
            [MOCHA_ID_PROP_NAME]: this.id,
            parent: this.parent
                ? { [MOCHA_ID_PROP_NAME]: this.parent.id }
                : null,
        };
    }
    static constants = constants;
}
