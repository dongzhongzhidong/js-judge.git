import {} from "mocha";
/** 通过一个接口转接 Context 给外部使用 */
let currentContext;
export const afterEach: Mocha.HookFunction = function (...args) {
    return (currentContext.afterEach || currentContext.teardown).apply(
        this,
        args
    );
};
export const after: Mocha.HookFunction = function (...args) {
    return (currentContext.after || currentContext.suiteTeardown).apply(
        this,
        args
    );
};
export const beforeEach: Mocha.HookFunction = function (...args) {
    return (currentContext.beforeEach || currentContext.setup).apply(
        this,
        args
    );
};
export const before: Mocha.HookFunction = function (...args) {
    return (currentContext.before || currentContext.suiteSetup).apply(
        this,
        args
    );
};
export const describe: Mocha.SuiteFunction = function (...args) {
    return (currentContext.describe || currentContext.suite).apply(this, args);
};
describe.only = function (...args) {
    return (currentContext.describe || currentContext.suite).only.apply(
        this,
        args
    );
};
describe.skip = function (...args) {
    return (currentContext.describe || currentContext.suite).skip.apply(
        this,
        args
    );
};
export const it = function (...args) {
    return (currentContext.it || currentContext.test).apply(this, args);
} as any as Mocha.TestFunction;
it.only = function (...args) {
    return (currentContext.it || currentContext.test).only.apply(this, args);
};
it.skip = function (...args) {
    return (currentContext.it || currentContext.test).skip.apply(this, args);
};
export const xdescribe = describe.skip;
export const xit = it.skip;
export const setup = beforeEach;
export const suiteSetup = before;
export const suiteTeardown = after;
export const suite = describe;
export const teardown = afterEach;
export const test = it;
export const injectContext = (context) => {
    currentContext = context;
};
