"use strict";

import { Runnable } from "./runnable";

/**
 * Initialize a new `Context`.
 *
 */
export class Context {
    _runnable: Runnable;
    test: Runnable;
    currentTest: Runnable;
    constructor() {}
    /**
     * Set or get the context `Runnable` to `runnable`.
     *
     * @private
     * @param {Runnable} runnable
     * @return {Context} context
     */
    runnable(): Runnable;
    runnable(runnable: Runnable): this;
    runnable(runnable?: Runnable) {
        if (!arguments.length) {
            return this._runnable;
        }
        this.test = this._runnable = runnable;
        return this;
    }
    /**
     * Set or get test timeout `ms`.
     *
     * @private
     * @param {number} ms
     * @return {Context} self
     */
    timeout(): number;
    timeout(ms: number): this;
    timeout(ms?: number): this | number {
        if (!arguments.length) {
            return this.runnable().timeout();
        }
        this.runnable().timeout(ms);
        return this;
    }
    /**
     * Set or get test slowness threshold `ms`.
     *
     * @private
     * @param {number} ms
     * @return {Context} self
     */
    slow(): number;
    slow(ms: number): Context;
    slow(ms?: number): Context | number {
        if (!arguments.length) {
            return this.runnable().slow();
        }
        this.runnable().slow(ms);
        return this;
    }
    /**
     * Mark a test as skipped.
     *
     * @private
     * @throws Pending
     */
    skip() {
        this.runnable().skip();
    }
    /**
     * Set or get a number of allowed retries on failed tests
     *
     * @private
     * @param {number} n
     * @return {Context} self
     */

    retries(): number;
    retries(n: number): Context;
    retries(n?: number): Context | number {
        if (!arguments.length) {
            return this.runnable().retries();
        }
        this.runnable().retries(n);
        return this;
    }
}
