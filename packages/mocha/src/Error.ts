class _Error extends Error {
    code?: string;
    interface: any;
    valueType: any;
    value: any;
    cleanReferencesAfterRun: boolean;
    instance: any;
    pluginDef: any;
    pluginImpl?: any;
    timeout: number;
    file: string;
    uncaught?: boolean;
    pattern?: string;
}
class _TypeError extends TypeError {
    code?: string;
    interface: any;
    valueType: any;
    value: any;
    cleanReferencesAfterRun: boolean;
    instance: any;
    pluginDef: any;
    pluginImpl?: any;
    timeout: number;
    file: string;
    uncaught?: boolean;
    pattern?: string;
    reporter?: string;
    argument?: string;
    expected?: string;
    actual?: string;
    reason?: string;
}

export { _Error as Error, _TypeError as TypeError };
