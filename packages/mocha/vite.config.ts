import { defineConfig } from "vite";
import visualizer from "rollup-plugin-visualizer";
import { nodePolyfills } from "vite-plugin-node-polyfills";

export default defineConfig((config) => {
    const { mode } = config;
    console.log(mode);
    return {
        plugins: [
            nodePolyfills({
                // Whether to polyfill `node:` protocol imports.
                protocolImports: true,
            }),
            mode === "analyze" &&
                (visualizer({
                    open: true,
                    filename: "visualizer/stat.html",
                }) as any),
        ],

        server: {
            port: 3000,
        },
        build: {
            lib: {
                entry: "./src/index.ts",
                formats: ["es"],
                fileName: "index",
            },
        },
    };
});
