import { BlocksToCode, useBlockly, initModuleBlocks } from "../src/index";
initModuleBlocks();
new Worker("./test/worker.ts", {
    type: "module",
});
// document.addEventListener("DOMContentLoaded", function () {
//     const { autoResize, onChange, loadFromString } = useBlockly(
//         document.querySelector("#blocklyDiv")!
//     );
//     loadFromString(localStorage.getItem("blockly_code")!);
//     onChange((file) => {
//         const old = JSON.parse(file);
//         console.log({ file, code: BlocksToCode(old) });
//         localStorage.setItem("blockly_code", file);
//     });
// });
