import { BlocksToCode, initModuleBlocks, InjectEnv } from "../src";
InjectEnv();
initModuleBlocks();
const data = await fetch("./index.block").then((res) => res.json());
const info = BlocksToCode(data);
console.log(info);
