import { utils } from "blockly";

// import document from "min-document";
import { DOMParser, XMLSerializer, DOMImplementation } from "@xmldom/xmldom";
export const InjectEnv = () => {
    const document = new DOMImplementation().createDocument(null, null, null);
    console.log(document);
    utils.xml.injectDependencies({
        document,
        DOMParser,
        XMLSerializer,
    });
};
