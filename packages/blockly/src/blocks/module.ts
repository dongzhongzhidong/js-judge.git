import Blockly, { Block, BlockSvg } from "blockly/core";
import { Names, Variables, Workspace } from "blockly";
import { javascriptGenerator as JavaScript } from "blockly/javascript";

const Msg = Blockly.Msg;
const Mutator = Blockly.Mutator;
const xmlUtils = Blockly.utils.xml;
const Align = Blockly.Input.Align;
const defineBlocksWithJsonArray = Blockly.defineBlocksWithJsonArray;
const NameType = Blockly.Names.NameType;

const Import = () => {
    defineBlocksWithJsonArray([
        {
            type: "module_import",
            message0: "从路径 %2 导入 %1 ",
            args0: [
                {
                    type: "field_variable",
                    name: "VAR",
                    variable: "%{BKY_VARIABLES_DEFAULT_NAME}",
                },
                {
                    type: "input_value",
                    name: "PATH",
                    check: "String",
                },
            ],
            previousStatement: null,
            nextStatement: null,
            style: "variable_blocks",
            tooltip: "%{BKY_VARIABLES_SET_TOOLTIP}",
            helpUrl: "%{BKY_VARIABLES_SET_HELPURL}",
            extensions: ["contextMenu_variableSetterGetter"],
        },
    ]);

    // 暂存 import 变量
    const ModuleVars = new Set();
    JavaScript.ModuleVars = ModuleVars;
    let finish = JavaScript.finish;
    JavaScript.finish = function () {
        ModuleVars.clear();
        return finish.apply(this, arguments);
    };
    /** 因为 blockly 预先将变量全部写好了，导致我们需要在后面进行重写覆盖 */
    JavaScript.redefineVars = function (workspace: Workspace) {
        let defvars = [];
        // Add developer variables (not created or named by the user).
        const devVarList = Variables.allDeveloperVariables(workspace);
        for (let i = 0; i < devVarList.length; i++) {
            defvars.push(
                this.nameDB_.getName(devVarList[i], NameType.DEVELOPER_VARIABLE)
            );
        }

        // Add user variables, but only ones that are being used.
        const variables = Variables.allUsedVarModels(workspace);
        for (let i = 0; i < variables.length; i++) {
            defvars.push(
                this.nameDB_.getName(variables[i].getId(), NameType.VARIABLE)
            );
        }
        defvars = defvars.filter((i) => !ModuleVars.has(i));
        // console.log(defvars);
        // Declare all of the variables.
        if (defvars.length) {
            this.definitions_["variables"] = "var " + defvars.join(", ") + ";";
        } else {
            this.definitions_["variables"] = "";
        }
    };
    JavaScript["module_import"] = function (block) {
        const path =
            JavaScript.valueToCode(block, "PATH", JavaScript.ORDER_NONE) || "";
        // Append to a variable in place.
        const varName = JavaScript.nameDB_.getName(
            block.getFieldValue("VAR"),
            Blockly.Names.NameType.VARIABLE
        );
        ModuleVars.add(varName);
        JavaScript.redefineVars(block.workspace);

        const code = `import {${varName}} from ${path};`;
        return code;
    };
};

export const initModuleBlocks = () => {
    Import();
    JavaScript["module_export_with"] = function (block) {
        // block.outputConnection = true;
        // Create a list with any number of elements of any type.
        const elements = new Array(block.itemCount_);
        for (let i = 0; i < block.itemCount_; i++) {
            elements[i] =
                JavaScript.valueToCode(
                    block,
                    "ADD" + i,
                    JavaScript.ORDER_NONE
                ) || null;
        }
        const code = "export {" + elements.filter((i) => i).join(", ") + "};";

        return code;
    };
    Blockly.Blocks["module_export_with"] = {
        /**
         * Block for creating a list with any number of elements of any type.
         * @this {Block}
         */
        init(this: Block) {
            this.setHelpUrl("导出变量");
            this.setStyle("list_blocks");
            (this as any).itemCount_ = 3;
            (this as any).updateShape_();
            // this.setPreviousStatement(null);
            this.suppressPrefixSuffix = true;
            this.setPreviousStatement(true);
            this.setOutput(false, null);
            this.setMutator(
                new Mutator(["lists_create_with_item"], this as BlockSvg)
            );

            this.setTooltip("导出变量");
        },
        /**
         * Create XML to represent list inputs.
         * Backwards compatible serialization implementation.
         * @return {!Element} XML storage element.
         * @this {Block}
         */
        mutationToDom: function () {
            const container = xmlUtils.createElement("mutation");
            container.setAttribute("items", this.itemCount_);
            return container;
        },
        /**
         * Parse XML to restore the list inputs.
         * Backwards compatible serialization implementation.
         * @param {!Element} xmlElement XML storage element.
         * @this {Block}
         */
        domToMutation: function (xmlElement) {
            this.itemCount_ = parseInt(xmlElement.getAttribute("items"), 10);
            this.updateShape_();
        },
        /**
         * Returns the state of this block as a JSON serializable object.
         * @return {{itemCount: number}} The state of this block, ie the item count.
         */
        saveExtraState: function () {
            return {
                itemCount: this.itemCount_,
            };
        },
        /**
         * Applies the given state to this block.
         * @param {*} state The state to apply to this block, ie the item count.
         */
        loadExtraState: function (state) {
            this.itemCount_ = state["itemCount"];
            this.updateShape_();
        },
        /**
         * Populate the mutator's dialog with this block's components.
         * @param {!Workspace} workspace Mutator's workspace.
         * @return {!Block} Root block in mutator.
         * @this {Block}
         */
        decompose: function (workspace) {
            const containerBlock = workspace.newBlock(
                "lists_create_with_container"
            );
            containerBlock.initSvg();
            let connection = containerBlock.getInput("STACK").connection;
            for (let i = 0; i < this.itemCount_; i++) {
                const itemBlock = workspace.newBlock("lists_create_with_item");
                itemBlock.initSvg();
                connection.connect(itemBlock.previousConnection);
                connection = itemBlock.nextConnection;
            }
            return containerBlock;
        },
        /**
         * Reconfigure this block based on the mutator dialog's components.
         * @param {!Block} containerBlock Root block in mutator.
         * @this {Block}
         */
        compose: function (containerBlock) {
            let itemBlock = containerBlock.getInputTargetBlock("STACK");
            // Count number of inputs.
            const connections = [];
            while (itemBlock) {
                if (itemBlock.isInsertionMarker()) {
                    itemBlock = itemBlock.getNextBlock();
                    continue;
                }
                connections.push(itemBlock.valueConnection_);
                itemBlock = itemBlock.getNextBlock();
            }
            // Disconnect any children that don't belong.
            for (let i = 0; i < this.itemCount_; i++) {
                const connection = this.getInput("ADD" + i).connection
                    .targetConnection;
                if (connection && connections.indexOf(connection) === -1) {
                    connection.disconnect();
                }
            }
            this.itemCount_ = connections.length;
            this.updateShape_();
            // Reconnect any child blocks.
            for (let i = 0; i < this.itemCount_; i++) {
                Mutator.reconnect(connections[i], this, "ADD" + i);
            }
        },
        /**
         * Store pointers to any connected child blocks.
         * @param {!Block} containerBlock Root block in mutator.
         * @this {Block}
         */
        saveConnections: function (containerBlock) {
            let itemBlock = containerBlock.getInputTargetBlock("STACK");
            let i = 0;
            while (itemBlock) {
                if (itemBlock.isInsertionMarker()) {
                    itemBlock = itemBlock.getNextBlock();
                    continue;
                }
                const input = this.getInput("ADD" + i);
                itemBlock.valueConnection_ =
                    input && input.connection.targetConnection;
                itemBlock = itemBlock.getNextBlock();
                i++;
            }
        },
        /**
         * Modify this block to have the correct number of inputs.
         * @private
         * @this {Block}
         */
        updateShape_: function () {
            if (this.itemCount_ && this.getInput("EMPTY")) {
                this.removeInput("EMPTY");
            } else if (!this.itemCount_ && !this.getInput("EMPTY")) {
                this.appendDummyInput("EMPTY").appendField(
                    Msg["LISTS_CREATE_EMPTY_TITLE"]
                );
            }
            // Add new inputs.
            for (let i = 0; i < this.itemCount_; i++) {
                if (!this.getInput("ADD" + i)) {
                    const input = this.appendValueInput("ADD" + i).setAlign(
                        Align.RIGHT
                    );
                    if (i === 0) {
                        input.appendField("导出变量");
                    }
                }
            }
            // Remove deleted inputs.
            for (let i = this.itemCount_; this.getInput("ADD" + i); i++) {
                this.removeInput("ADD" + i);
            }
        },
    };
};
