import { Workspace } from "blockly";
import Blockly from "blockly/core";
import { javascriptGenerator } from "blockly/javascript";
export const BlocksToCode = (blocks: any) => {
    const w = new Workspace();
    Blockly.serialization.workspaces.load(blocks, w);
    return javascriptGenerator.workspaceToCode(w);
};
