import * as Blockly from "blockly";
import { javascriptGenerator } from "blockly/javascript";
import * as ZH from "blockly/msg/zh-hans";
Blockly.setLocale(ZH);
export * from "./compile";
export * from "./useBlockly";
export * from "./blocks/module";
export { Blockly, javascriptGenerator };
export { InjectEnv } from "./WorkSpace";
