import * as Blockly from "blockly";
import { javascriptGenerator } from "blockly/javascript";
import toolbox from "./toolbox.xml?raw";
import Theme from "./theme";
export const useBlockly = (
    element: Element,
    options?: Blockly.BlocklyOptions
) => {
    const workspace = Blockly.inject(element, {
        comments: true,
        collapse: true,
        disable: true,
        grid: {
            spacing: 25,
            length: 3,
            colour: "#ccc",
            snap: true,
        },
        theme: Theme,
        media: "https://cdn.jsdelivr.net/npm/blockly/media/",
        toolbox: toolbox,
        zoom: {
            controls: true,
            wheel: false,
            startScale: 1.0,
            maxScale: 4,
            minScale: 0.25,
            scaleSpeed: 1.1,
        },

        ...options,
    });
    const getCode = () => {
        return javascriptGenerator.workspaceToCode(workspace);
    };

    const loadFromString = (string?: string) => {
        try {
            const old = JSON.parse(string);
            Blockly.serialization.workspaces.load(old, workspace);
        } catch (e) {}
    };

    return {
        Blockly,
        workspace,
        getCode,
        autoResize() {
            const ro = new ResizeObserver((entries) => {
                //TODO 添加节流
                console.log("重置大小");
                Blockly.svgResize(workspace);
            });
            ro.observe(element);
            return () => {
                ro.disconnect();
            };
        },
        loadFromString,
        getFile() {
            return JSON.stringify(
                Blockly.serialization.workspaces.save(workspace)
            );
        },
    };
};
