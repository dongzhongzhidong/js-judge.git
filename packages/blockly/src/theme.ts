import * as Blockly from "blockly";
import Color from "./open-color";

const names = [
    ["logic", "red"],
    ["loop", "orange"],
    ["math", "lime"],
    ["text", "teal"],
    ["list", "cyan"],
    ["colour", "blue"],
    ["variable", "violet"],
    ["procedure", "pink"],
] as [string, keyof typeof Color][];

/**
 * Dark theme.
 */
export default Blockly.Theme.defineTheme("good", {
    name: "good",
    base: Blockly.Themes.Classic,
    fontStyle: {
        family: "Georgia, serif",
        weight: "bold",
        size: 12,
    },
    blockStyles: {
        ...(Object.fromEntries(
            names.map(([name, color]) => {
                return [
                    name + "_blocks",
                    {
                        colourPrimary: Color[color][500],
                        colourSecondary: Color[color][500],
                        colourTertiary: Color[color][500],
                    },
                ];
            })
        ) as any),
    },
    categoryStyles: {
        ...(Object.fromEntries(
            names.map(([name, color]) => {
                return [name + "_category", { colour: Color[color][600] }];
            })
        ) as any),
    },
    componentStyles: {
        workspaceBackgroundColour: "#fff",
        toolboxBackgroundColour: "#f5f5f5",
        toolboxForegroundColour: "#000",
        flyoutBackgroundColour: "#666",
        flyoutForegroundColour: "#ccc",
        flyoutOpacity: 0.8,
        scrollbarColour: "#797979",
        insertionMarkerColour: "#fff",
        insertionMarkerOpacity: 0.3,
        scrollbarOpacity: 0.3,
        cursorColour: "#d0d0d0",
    },
});
