import { defineConfig } from "vite";
import visualizer from "rollup-plugin-visualizer";
import { nodePolyfills } from "vite-plugin-node-polyfills";

export default defineConfig((config) => {
    const { mode } = config;
    console.log(mode);
    return {
        plugins: [
            mode === "analyze" &&
                (visualizer({
                    open: true,
                    filename: "visualizer/stat.html",
                }) as any),
        ],

        server: {
            port: 3000,
        },
        build: {
            rollupOptions: {
                external: [
                    "blockly",
                    "blockly/core",
                    "blockly/javascript",
                    "blockly/msg/zh-hans",
                    "@xmldom/xmldom",
                ],
            },
            lib: {
                entry: "./src/index.ts",
                formats: ["es"],
                fileName: "index",
            },
        },
    };
});
